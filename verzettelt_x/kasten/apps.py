from django.apps import AppConfig


class KastenConfig(AppConfig):
    name = 'kasten'
