from django.http.response import HttpResponse
from django.shortcuts import render, HttpResponse, redirect

from kasten.models import Note, Tag

# Create your views here.

def index(request):
    notes = Note.objects.order_by('-date_created')[:6]
    tags = Tag.objects.all()
   # taged_note = Note.objects.get()   
   # tags = taged_note.tags.all() ---Versuch auf tags zuzugreifen
   #{% url 'note' %}?={{note.id}}</div> ---Versuch note mittels id zu verlinken

    if request.method == 'POST':
        note = Note.objects.create(
            title = request.POST.get('body'),
            content = request.POST.get('content'),
         #   tag = request.POST.get('tag'), #Soll im html als auswahlinput abgebildet werden bestehende Tags können hier vergeben werden
            due_date = request.POST.get('due_date'),
        )
        return redirect('index')
    return render(request, "kasten/index.html", {'notes' : notes, 'tags':tags})
    
def note(request, pk):
    note = Note.objects.get(id=pk) 
    tags = note.tags.all()
    return render(request, 'kasten/note.html', {'note': note, 'tags':tags})

def tagedNotes(request, pk):
    tag = Tag.objects.get(id=pk)
    notes = Note.objects.get(tags=tag) # tag in tags???
    context = {
        'note': notes,
        'tag': tag
         }
    return render(request, 'kasten/taged_notes.html', context)

def notesList(request):
    notes = Note.objects.all()
    tags = Tag.objects.all()
    return render(request, "kasten/notes_list.html", {'notes' : notes, 'tags':tags})

