from django.shortcuts import render, get_object_or_404, HttpResponseRedirect, redirect
from . models import Leaderboard, Score
from django.urls import reverse
import json
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse, HttpResponse
# Create your views here.

def index(request):
    leaderboards = Leaderboard.objects.order_by('date_created')
    context = {
        'leaderboards':leaderboards
    }
    return render (request, 'highscore/index.html', context)

def leaderboard(request, pk):
    board = Leaderboard.objects.get(id=pk)
    scores = Score.objects.filter(leaderboard_id=pk)[:10]
    context = {
        'board': board,
        'scores':scores
    }
    return render (request, 'highscore/board.html', context)

def score_entry(request, pk):
    board = Leaderboard.objects.get(id=pk)
    context = {
        'board': board
    }
    return render(request, 'highscore/score_entry.html', context)

def new_entry(request, pk):
    player_name = request.POST["player_name"]
    score = request.POST["score"]
    if score == "":
        score = 0
    playtime = request.POST["playtime"]
    leaderboard = Leaderboard.objects.get(id=pk)
    Score.objects.create(player_name=player_name,
    score=score,
    playtime=playtime,
    submission_type = 'M',
    leaderboard=leaderboard)
    return redirect('highscore:leaderboard', pk)
    return HttpResponseRedirect(reverse('leaderboard/', pk))


def new_board(request):
    return render(request, 'highscore/new_board.html')

def save_board(request):
   # game_title = request.POST["game_title"]
  #  game_subtitle = request.POST["game_subtitle"]
  #  note = request.POST["note"]
  #  game_url = request.POST["game_url"]
   # edit_pw = request.POST["edit_pw"]
  #  submit_pw = request.POST["submit_pw"]
    

   # Leaderboard.objects.create(game_title=game_title,
   #             game_subtitle=game_subtitle,
   #             note=note,
            
   #             game_url=game_url,
   #             edit_pw=edit_pw,
   #             submit_pw=submit_pw)
    leaderboard = Leaderboard()
    
    leaderboard.game_title = request.POST["game_title"]
    leaderboard.game_subtitle = request.POST["game_subtitle"]
    leaderboard.note = request.POST["note"]
    leaderboard.game_url = request.POST["game_url"]
    leaderboard.edit_pw = request.POST["edit_pw"]
    leaderboard.submit_pw = request.POST["submit_pw"]
    #leaderboard.
    leaderboard.save()
    return redirect('highscore:index')


  



@csrf_exempt
def api_v1_boards(request, board_id, password):
    board = get_object_or_404(Leaderboard, id=board_id, password=password)
    if request.POST:
        # A new score is submitted
        payload = json.loads(request.body)
        score = Score()
        score.board = board
        if "name" in payload:
            score.player_name = payload["name"]
        if "time" in payload:
            score.playtime = payload["time"]
        if "score" in payload:
            score.score = payload["score"]
        score.submission_type = "A"
        score.save()
        response = {"result": "ok"}
        return JsonResponse(response)
    else:
        # GET request, we return the current highscores
        response = {
            "title": board.game_title,
            "description": board.game_subtitle,
            "created": board.date_created,
            "scores": [],
                }
        for score in board:
            response["scores"].append({
                    "name": score.player_name,
                    "score": score.score,
                    "time": score.playtime,
                    "created": score.date_created,
                })
        return JsonResponse(response)