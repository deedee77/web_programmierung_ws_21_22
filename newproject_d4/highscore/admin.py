from django.contrib import admin

# Register your models here.


from .models import Leaderboard, Score


admin.site.register(Leaderboard)
admin.site.register(Score)