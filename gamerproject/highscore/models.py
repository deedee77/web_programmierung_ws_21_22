from django.db import models


# Create your models here.


class Leaderboard(models.Model):
    submit_pw = models.CharField(max_length=50)
    edit_pw = models.CharField(max_length=50)
    date_created = models.DateTimeField()
    game_title = models.CharField(max_length=75)
    game_subtitle = models.CharField(max_length=150, blank=True, null=True)
    note = models.TextField(blank=True, null=True)
    game_url = models.URLField(blank=True, null=True)
    
    def __str__(self):
        return f"{self.game_title} {self.game_subtitle} {self.note}"

class Score(models.Model):
    SUBMISSION_TYPES = (
        ('A', 'API'),
        ('M', 'manually'),
    )
    submission_type = models.CharField(max_length=1, choices=SUBMISSION_TYPES)
    player_name = models.CharField(max_length=50, default='Anonymous')
    score = models.IntegerField()
    playtime = models.CharField(max_length= 30, default="00:00:00")
    leaderboard = models.ForeignKey(Leaderboard, on_delete=models.CASCADE)
    date_created = models.DateTimeField(auto_now_add=True)
    #additional_data = models.JSONField(encoder=None, decoder=None, blank=True, null=True)
    additional_data = models.TextField(blank=True, null=True)

    class Meta:
        ordering = ['-score']

    def __str__(self):
        return f"{self.player_name} {self.leaderboard}"
        