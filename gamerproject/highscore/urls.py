"""gamerproject URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from . import views

app_name = 'highscore'
urlpatterns = [
   path('', views.index, name='index'),
   path('leaderboard/<int:pk>/', views.leaderboard, name='leaderboard'),
   path('new_board/', views.new_board, name='new_board'),
   path('score_entry/<int:pk>/', views.score_entry, name='score_entry'),
   path('new_entry/<int:pk>/', views.new_entry, name='new_entry'),
   path('save_board/<int:pk>/', views.save_board, name='save_board'),
]
