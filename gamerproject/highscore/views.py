from django.shortcuts import render, get_object_or_404, HttpResponseRedirect, redirect
from . models import Leaderboard, Score
from django.urls import reverse

# Create your views here.

def index (request):
    leaderboards = Leaderboard.objects.order_by('date_created')
    context = {
        'leaderboards':leaderboards
    }
    return render (request, 'highscore/index.html', context)

def leaderboard(request, pk):
    board = Leaderboard.objects.get(id=pk)
    
    scores = Score.objects.filter(leaderboard_id=pk)
    context = {
        'board': board,
        'scores':scores
    }
    return render (request, 'highscore/board.html', context)

def score_entry(request, pk):
    board = Leaderboard.objects.get(id=pk)
    context = {
        'board': board
    }
    return render(request, 'highscore/score_entry.html', context)

def new_entry(request, pk):
    player_name = request.POST["player_name"]
    score = request.POST["score"]
    playtime = request.POST["playtime"]
    leaderboard = Leaderboard.objects.get(id=pk)
    Score.objects.create(player_name=player_name,
    score=score,
    playtime=playtime,
    submission_type = 'M',
    leaderboard=leaderboard)
    #return redirect('leaderboard/'+ str(pk))
    return HttpResponseRedirect('leaderboard/'+ str(pk))


def new_board(request):
    return render(request, 'highscore/new_board.html')

def save_board(request):
    game_title = request.POST["game_title"]
    game_subtitle = request.POST["game_subtitle"]
    note = request.POST["note"]
    game_url = request.POST["game_url"]
    edit_pw = request.POST["edit_pw"]
    submit_pw = request.POST["submit_pw"]
    date_created= "2020-01-01 09:17"

    Leaderboard.objects.create(game_title=game_title,
                game_subtitle=game_subtitle,
                note=note,
                date_created= date_created,
                game_url=game_url,
                edit_pw=edit_pw,
                submit_pw=submit_pw)
    return HttpResponseRedirect(reverse('index'))


  

