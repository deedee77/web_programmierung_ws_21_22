from django.db import models

# Create your models here.
class List(models.Model):
    name = models.CharField(max_length=250)
    description = models.TextField
    pub_date = models.DateTimeField('date published')

    def __str__(self):
          return self.name

class ToDoItem(models.Model):
    list = models.ForeignKey(List, on_delete=models.CASCADE)
    text = models.CharField(max_length=250)
    description = models.TextField
    done = models.BooleanField(default=False)

    def __str__(self):
          return f"{self.text} {self.done}"
