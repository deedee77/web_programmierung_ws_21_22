from typing import List

from django.http import HttpResponse

from django.shortcuts import get_object_or_404, render, HttpResponseRedirect

from .models import List, ToDoItem

from django.template import loader

from django.urls import reverse

# Create your views here.

def list(request):
    lists_list = List.objects.order_by('name')
    template = loader.get_template('list/list.html')
    context = {
        'lists_list': lists_list,
    }
    return HttpResponse(template.render(context, request))

def detail(request, list_id):
    list = get_object_or_404(List, pk=list_id)
    return render(request, 'list/detail.html', {'list': list})

def status_change(request, todoitem_id):
    todo = get_object_or_404(TodoItem, pk=todoitem_id)
    if todo.done == True:
        todo.done = False
    else:
        todo.done = True
    todo.save()
    return HttpResponseRedirect(reverse('list:detail'))

