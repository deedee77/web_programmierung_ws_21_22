from django.contrib import admin
from . import models

# Register your models here.
admin.site.register(models.Publisher)
admin.site.register(models.Person)
admin.site.register(models.Band)
admin.site.register(models.Album)
admin.site.register(models.Book)
admin.site.register(models.Journal)
admin.site.register(models.Item)
admin.site.register(models.Track)










