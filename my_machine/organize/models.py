from multiprocessing import AuthenticationError
from unicodedata import name
from django.db import models

# Create your models here.

class Publisher(models.Model):
    name = models.CharField(max_length=150)
    def __str__(self):
            return self.name

class Person(models.Model):
    surname =  models.CharField(max_length=100)
    firstname = models.CharField(max_length=100)
    date_of_birth =  models.DateField(blank = True, null= True) # ist es hier möglich eine alternative für nur das Jahr anzugeben?
    date_of_death =  models.DateField(blank = True, null= True)
    def __str__(self):
            return self.surname

class Band(models.Model):
    name = models.CharField(max_length=255)
    members = models.ManyToManyField(Person) # knann hier noch eine Rolle (Sänger, Bassist etc.) übergeben werden ?
    established = models.IntegerField(null = True, blank = True)
    def __str__(self):
            return self.name

class Resource(models.Model):
    title = models.CharField(max_length=255)
    subtitel = models.CharField(max_length=355, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    entry_date = models.DateTimeField(auto_now_add=True)
    language = models.CharField(max_length = 3, null=True, blank=True)
    uri = models.CharField(max_length= 150, null=True, blank=True)
    publisher = models.ManyToManyField(Publisher, null=True, blank=True)
    info_updated = models.DateTimeField(auto_now=True)
    def __str__(self):
        return self.title
    
    class Meta:
        abstract = True

class Text(Resource):
    author = models.ManyToManyField(Person)
    abstract = models.TextField(null=True, blank=True)
    excerpt = models.TextField(null=True, blank=True)

    class Meta:
        abstract = True

class Audio(Resource):
    abstract = models.TextField(null=True, blank=True)
    excerpt = models.TextField(null=True, blank=True)
    class Meta:
        abstract = True
class Album(Audio):
    band = models.ManyToManyField(Band)

class Track(Audio):
    album = models.ManyToManyField(Album, null = True, blank=True)
    duration = models.IntegerField( null = True, blank=True) # Angabe in sec
    performer = models.ManyToManyField(Person, null = True, blank=True)
    band = models.ManyToManyField(Band, null = True, blank=True)

class Book(Text):
    isbn = models.CharField(max_length=13, null=True, blank=True)

class Journal(Text):
    issn = models.CharField(max_length=13, null=True, blank=True)


    
class Item(Text):
    book = models.ForeignKey(Book, on_delete= models.DO_NOTHING, null = True, blank = True)
    journal = models.ForeignKey(Journal, on_delete= models.DO_NOTHING, null = True, blank = True)



#class quotation(models.Model):
  #  text = models.TextField
   # resource = models.ForeignKey(Resource, on_delete=models.DO_NOTHING)
   # location = models.CharField(max_length= 20,blank=True, null=True)   ---> Foreign key kann nicht auf eine Superclasse verweisen