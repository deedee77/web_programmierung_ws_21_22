from django.shortcuts import render, HttpResponse
from . import models

# Create your views here.
def index(request):
   # tester = models.Resource.objects.all()
    books = models.Book.objects.all()[:5]
    albums = models.Album.objects.all()[:5]
    context = { 'books': books, 'albums': albums}
    
    return render(request, 'organize/index.html', context)