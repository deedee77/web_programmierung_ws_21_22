from django.shortcuts import render, get_object_or_404, HttpResponseRedirect, redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader
from django.urls import reverse

from .models import Shop, Product

# Create your views here.

def shops(request):
    shop_list = Shop.objects.order_by('name')
    template = loader.get_template('ek_list/shops.html')
    context = {
        'shop_list': shop_list,
    }
    return HttpResponse(template.render(context, request))

def list(request, shop_id):
    shop = get_object_or_404(Shop, pk=shop_id)
    return render(request, 'ek_list/list.html', {'shop': shop, 'status_choices' : Product.status_choices, 'shops' : Shop.objects.all()})


def status_change(request, product_id):
    product = get_object_or_404(Product, pk=product_id)
    product.status = request.POST["status"] 
    product.save()
    shop_id= product.shop.id
    return redirect('ek_list:list', product.shop.id)
    

def delete_item(request, product_id):
    product = get_object_or_404(Product, pk=product_id)
    get_object_or_404(Product, pk=product_id).delete()
    return redirect('ek_list:list', product.shop.id)

def delete_shop(request, shop_id):
    get_object_or_404(Shop, pk=shop_id).delete()
    return HttpResponseRedirect(reverse ('ek_list:shops'))



    
def new(request):
    name = request.POST["name"]
    note = request.POST["note"]
    amount = request.POST["amount"]
    unit = request.POST["unit"]
    price = request.POST["price"]
    status = request.POST["status"]
    shop = Shop.objects.get(pk=request.POST["shop"])

    print(name, note, amount, unit, price, status, shop)

    Product.objects.create(name=name,
                        note=note,
                        amount=amount,
                        unit=unit,
                        price=price,
                        status = status,
                        shop=shop)
    return HttpResponseRedirect(reverse('ek_list:shops'))
    

def newshop(request):
    name = request.POST["name"]


    print(name)

    Shop.objects.create(name=name)
    return HttpResponseRedirect(reverse('ek_list:shops'))