"""einkaufsliste URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from . import views
from django.urls import path

app_name = "ek_list"
urlpatterns = [
    path('', views.shops, name='shops'),
    path('<int:shop_id>/', views.list, name='list'),
    path('<int:product_id>/status_change', views.status_change, name="status_change"),
    path('new', views.new, name="new"),
    path('newshop', views.newshop, name="newshop"),
    path('<int:product_id>/delete_item', views.delete_item, name="delete_item"),
    path('<int:shop_id>/delete_shop', views.delete_shop, name="delete_shop"),
]
