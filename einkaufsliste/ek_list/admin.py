from django.contrib import admin

# Register your models here.


from .models import Shop, Product


class ProductInline(admin.TabularInline):
    model = Product
    
class ShopAdmin(admin.ModelAdmin):
    inlines = [ProductInline]


admin.site.register(Shop, ShopAdmin)
admin.site.register(Product)

