from django.db import models

# Create your models here.

class Shop(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return f"{self.name}"



class Product(models.Model):
    status_choices = [
        ("D", "Done"),
        ("B", "Buy"),
        ("C", "Canceled")
    ]

    name = models.CharField(max_length=100)
    note = models.CharField(max_length=500)
    amount = models.IntegerField(default = 1)
    unit = models.CharField(max_length=15, default = "")
    price = models.FloatField(default = 0.00)
    status = models.CharField(max_length=1, choices=status_choices, default="B")
    shop = models.ForeignKey(Shop, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.name} {self.shop} {self.status} {self.amount} {self.unit} {self.price}"